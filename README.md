# TP-DeviceDrivers



## Moodulo Hola Mundo

```
Precondición:

En primer lugar se verificó la versión del Kernel presente con el comando uname -r.

Una vez obtenida la versión procedemos a la instalación de los paquetes que necesitamos para trabajar (module-init-header, linux-heaers-$(uname -r))
```


## Desarrollo del Ejercicio "Hola Mundo"

```
Se clonó desde el repositorio que se compartío en el enunciado del TP.
Dicho proyecto contenia inicialmente un archivo Makefile que compilaba el archivo miModulo.c.

Una vez clonado el repositorio, se ingresó a la terminal y se ingresa el siguiente comando:

sumo E- make

Dicho comando compilará el archivo Makefile creando nuestro archivo objeto "miModulo.o" y "miModulo.ko"


```
